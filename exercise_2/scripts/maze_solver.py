#!/usr/bin/env python
import rospy
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Twist
from tf import TransformListener

# Current robot name
robot_name = "amigobot1"

# Robot's base_link frame
base_link_frame = robot_name + "/base_link"

# Robot's odom frame
odom_frame = robot_name + "/odom"

# Sonar data subscriber
sub_sonar = None

# Robot velocity publisher
pub_velocity = None

# ROS tf listener
listener = None

def main() -> None:
    global sub_sonar, pub_velocity, listener

    # ROS node initialization
    rospy.init_node("amigobot_ex_2")

    # Init your subscribers and publishers here
    pass

    # Wait one second so all subscribers/listeners can start properly
    rospy.sleep(1)


if __name__ == "__main__":
    main()

