#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

using namespace std;

/**
 * @brief robot_name                Current robot name.
 */
const string robot_name="amigobot1";

/**
 * @brief base_link_frame           Robot's base_link frame.
 */
const string base_link_frame = robot_name + string("/base_link");

/**
 * @brief odom_frame                Robot's odom frame.
 */
const string odom_frame = robot_name + string("/odom");

/**
 * @brief sub_sonar                 Sonar data subscriber.
 */
ros::Subscriber sub_sonar;

/**
 * @brief pub_velocity              Robot velocity publisher.
 */
ros::Publisher pub_velocity;

/**
 * @brief listener                  ROS tf listener.
 */
tf::TransformListener* listener;

/**
 * @brief main                      Main routine.
 * @param argc                      Used as default in ros.
 * @param argv                      Used as default in ros.
 * @return
 */
int main(int argc, char** argv)
{
    //ROS node initialization
    ros::init(argc, argv, "amigobot_ex_2");
    ros::NodeHandle n;

    // Init your subscribers and publishers here


    // Run asynchronous spinner, so callbacks can run in background
    ros::AsyncSpinner spinner(1);
    spinner.start();

    // Wait one second so all subscribers/listeners can start properly
    ros::Duration(1).sleep();

    return 0;
}
