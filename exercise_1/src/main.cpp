#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Twist.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/**
 * @brief robot_name                Current robot name.
 */
const string robot_name="amigobot1";

/**
 * @brief sub_sonar                 Sonar data subscriber.
 */
ros::Subscriber sub_sonar;

/**
 * @brief pub_velocity              Robot velocity publisher.
 */
ros::Publisher pub_velocity;

/**
 * @brief getNextDestination        Calculates the next robot goal in local coordinate system
 *                                  (it works like a carrot on the stick).
 * @param sonar                     The ROS sonar data.
 * @return                          The destination coordinates.
 */
geometry_msgs::Point32 getNextDestination(const sensor_msgs::PointCloud& sonar){
    return geometry_msgs::Point32();
}

/**
 * @brief wallAtFront               Checks whether there is a wall in front of the robot.
 * @param sonar                     The ROS sonar data.
 * @return                          True if the wall is present, false if the way is free.
 */
bool wallAtFront(const sensor_msgs::PointCloud& sonar){
    return false;
}

/**
 * @brief PID                       Simple PID driver.
 * @param u                         Driver input signal.
 * @return                          Driver output signal.
 */
float PID(float u){
    return 0;
}

/**
 * @brief move                      Send the move forward command to the robot, with given
 *                                  additional rotation.
 * @param rotation                  Rotation speed, from -1 to 1.
 */
void move_robot(float rotation){
}

/**
 * @brief stop                      Send the stop command to the robot.
 */
void stop(){
}

/**
 * @brief drawDestination           Debug function drawing the robot move direction.
 * @param output                    Visualization output image.
 * @param p                         Calculated destination.
 */
void drawDestination(Mat& output, geometry_msgs::Point32 p){
    output.setTo(Scalar(255,255,255));
    Point p0(output.cols/2, output.rows-1);
    Point p1((1-p.y/0.1)*output.cols/2,
             (1-p.x/0.1)*output.rows);
    line(output, p0, p1, Scalar(20,200,20), 5);
}

/**
 * @brief sonarCallback             Process sonar data.
 * @param msg                       ROS sonar message.
 */
void sonarCallback(const sensor_msgs::PointCloud& msg )
{
    // Static variable to stop the robot
    static bool keep_moving = false;

    // Calculate next destination of the robot
    geometry_msgs::Point32 dest = getNextDestination(msg);

    // Preview of the destination
    Mat output(100,200,CV_8UC3);
    drawDestination(output, dest);
    imshow("Output", output);

    // ESC exits the program
    char zn = waitKey(20);
    if(zn == 27){
        exit(0);
    }
    //Space pauses/resumes the robot
    if(zn == ' '){
        keep_moving = !keep_moving;
    }
    if(!keep_moving){
        stop();
        return;
    }

    //If robot is in front of the wall
    if( 1/*warunek*/ ){
        // STAHP THE ROBOT
    }
    else{
        //Calculate steering
    }
}

/**
 * @brief main                      Main routine.
 * @param argc                      Used as default in ros.
 * @param argv                      Used as default in ros.
 * @return
 */
int main(int argc, char** argv)
{
    //ROS node initialization
    ros::init(argc, argv, "amigobot_ex_1");
    ros::NodeHandle n;
    ros::Rate rate(30);

    // Subscriber and publisher init
    sub_sonar = n.subscribe(robot_name + string("/aria/sonar"), 10, &sonarCallback);
    pub_velocity = n.advertise<geometry_msgs::Twist>(robot_name + string("/aria/cmd_vel"), 10);

    while(n.ok())
    {
        //Spin
        ros::spinOnce();

        //Sleep
        rate.sleep();
    }
    return 0;
}
