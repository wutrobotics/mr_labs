#!/usr/bin/env python
import rospy
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Twist, Point32
import cv2
import numpy as np

# Current robot name
robot_name: str = "amigobot1"

# Sonar data subscriber
sub_sonar = None  # type: rospy.Subscriber

# Robot velocity publisher
pub_velocity = None  # type: rospy.Publisher

# Global variable to simulate static variable in sonarCallback
keep_moving: bool = False

def getNextDestination(sonar: PointCloud) -> Point32:
    # Calculates the next robot goal in local coordinate system
    # (it works like a carrot on the stick).
    return Point32()

def wallAtFront(sonar: PointCloud) -> bool:
    # Checks whether there is a wall in front of the robot.
    return False

def PID(u: float) -> float:
    # Simple PID driver.
    return 0.0

def move(rotation: float) -> None:
    # Send the move forward command to the robot, with given additional rotation.
    pass

def stop() -> None:
    # Send the stop command to the robot.
    pass

def drawDestination(output: np.ndarray, p: Point32) -> None:
    # Debug function drawing the robot move direction.
    output.fill(255)
    p0 = (output.shape[1] // 2, output.shape[0] - 1)
    p1 = ((1 - p.y / 0.1) * output.shape[1] // 2, (1 - p.x / 0.1) * output.shape[0])
    cv2.line(output, p0, (int(p1[0]), int(p1[1])), (20, 200, 20), 5)

def sonarCallback(msg: PointCloud) -> None:
    # Process sonar data.
    global keep_moving

    # Calculate next destination of the robot
    dest: Point32 = getNextDestination(msg)

    # Preview of the destination
    output: np.ndarray = np.zeros((100, 200, 3), dtype=np.uint8)
    drawDestination(output, dest)
    cv2.imshow("Output", output)

    # ESC exits the program
    zn = cv2.waitKey(20)
    if zn == 27:
        exit(0)
    # Space pauses/resumes the robot
    if zn == ord(' '):
        keep_moving = not keep_moving
    if not keep_moving:
        stop()
        return

    # If robot is in front of the wall
    if wallAtFront(msg):
        # STAHP THE ROBOT
        pass
    else:
        # Calculate steering
        pass

def main() -> None:
    global sub_sonar, pub_velocity

    # ROS node initialization
    rospy.init_node("amigobot_ex_1")

    # Subscriber and publisher init
    sub_sonar = rospy.Subscriber(robot_name + "/aria/sonar", PointCloud, sonarCallback)
    pub_velocity = rospy.Publisher(robot_name + "/aria/cmd_vel", Twist, queue_size=10)

    rospy.spin()

if __name__ == "__main__":
    main()

